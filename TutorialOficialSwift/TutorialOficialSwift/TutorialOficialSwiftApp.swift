//
//  TutorialOficialSwiftApp.swift
//  TutorialOficialSwift
//
//  Created by victor manzanero on 25/11/23.
//

import SwiftUI

@main
struct TutorialOficialSwiftApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
